
import requests

class DjangoServer:

    def __init__(self):
        self.ip = '127.0.0.1'

    def is_white(self, client_mac):
        try:
            response_rest = requests.get('http://127.0.0.1:8000/api/machine/is_white/{0}'.format(client_mac)).json()
            is_white = response_rest["data"]["exist"]
            return is_white
        except:
            return False

    def need_reload(self, client_mac):
        response_rest = requests.get("http://127.0.0.1:8000/api/machine/enable_machine/{0}".format(client_mac)).json()
        return response_rest["data"]["needReload"]

    def disable_machine(self, client_mac):
        requests.post("http://127.0.0.1:8000/api/machine/disable_machine/{0}" .format(client_mac))

    def get_lease(self):
        return requests.get("http://127.0.0.1:8000/api/machine/lease").json()["data"]