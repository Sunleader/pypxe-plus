#!/bin/bash
rm -f /opt/pypxe-plus/py-tpid
source venv-linux/bin/activate
nohup python -m pypxe.server --config=config.json --debug all --verbose all > py.log 2>&1 &
echo $! > /opt/pypxe-plus/py-tpid
echo Start Success!
