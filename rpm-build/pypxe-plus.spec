name:       pypxe-plus
Version:    %{getenv:VERSION}
Release:    %{RELEASE}
Summary:    build pypxe-plus rpm
AutoReqProv:	no

Group:      pypxe-plus
License:    GPL

BuildRoot:  ~/rpmbuild/

%define __os_install_post %{nil}

%description
install pypxe plus

%prep
exit

%build

%install
mkdir -p %{buildroot}/usr/lib/systemd/system  
mkdir -p %{buildroot}/opt/pypxe-plus

cp %{_builddir}/pypxe-plus/pypxe-plus.service %{buildroot}/usr/lib/systemd/system/
cp %{_builddir}/pypxe-plus/pypxe.service %{buildroot}/usr/lib/systemd/system/

cp -r %{_builddir}/pypxe-plus/* %{buildroot}/opt/pypxe-plus/

chmod -R 777 %{buildroot}/opt/pypxe-plus/*

%files
%defattr(-,root,root,-)
/usr/lib/systemd/system/*
/opt/pypxe-plus/*

%post
systemctl daemon-reload
systemctl enable pypxe-plus
systemctl enable pypxe

%preun
systemctl stop pypxe-plus
systemctl stop pypxe
systemctl disable pypxe-plus
systemctl disable pypxe

%postun
systemctl daemon-reload


%clean
rm -rf %{_builddir}/%{name}-%{version}
rm -rf %{buildroot}/
