#!/bin/bash
rm -f /opt/pypxe-plus/tpid
#export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.8.10-1.el7.x86_64
#source /etc/profile
#export PATH=$JAVA_HOME/bin:$PATH
nohup jdk11/bin/java -Xms128m -Xmx512m -jar /opt/pypxe-plus/net-boot-0.0.1-SNAPSHOT.jar > plus.log 2>&1 &
echo $! > /opt/pypxe-plus/tpid
echo Start Success!
